# Simulador de robots basados en Arduino

Este simulador presenta los siguientes componentes:
 - Compilador: traduce el fichero con las instrucciones del robot a Python
 - Interfaz gráfica: permite elegir el fichero a compilar y muestra el comportamiento del robot

Para abrir la interfaz es necesario ejecutar el fichero Circuito.Python

Archivos del proyecto:
 - Lexer.py: analizador léxico del Compilador
 - Parser.py: analizador sintáctico del Compilador
 - Clases.py: traduce las reglas del parser a lenguaje Python
 - Circuito.py: interfaz gráfica, ejecuta la compilación del código y la simulación del robot
 - globals.py: variables compartidas por la interfaz y el fichero traducido
