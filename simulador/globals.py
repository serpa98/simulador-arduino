from Circuito import Desplazamiento
import pygame

# Tamaño del lienzo
resolucion = (1300, 630)

screen = pygame.display.set_mode(resolucion)

def cambia_pantalla(resolucion):
    screen = pygame.display.set_mode(resolucion)

# Establece el tamaño de los nodos
dist = 8

# Nodos y relaciones entre nodos del circuito
lista_nodos = []
lista_relaciones = []
for i in range(8):
    lista_relaciones.append([])

# Clase encargada del desplazamiento del vehiculo por el circuito
desplazamiento = None

# Botones de la interfaz
boton_nodo             = None      # Nuevo nodo
boton_fichero          = None      # Leer fichero
boton_simulacion       = None      # Simulacion
boton_siguiente_paso   = None      # Siguiente paso
boton_restaurar        = None      # Restaurar
boton_borrar_nodos     = None      # Borrar nodos
boton_cancelar         = None      # Cancelar borrar nodos
boton_si               = None      # Ejecucion paso a paso activada
boton_no               = None      # Ejecucion paso a paso desactivada

# Ejecucion realizada paso a paso o no
paso_a_paso = False

# Fichero python generado, usado para que la funcion espera()
# identifique la instruccion de la linea anterior y asi establecer
# la posicion de la ejecucion del programa
fich_py = ""

# Lista que guarda las coordenadas de cada linea del fichero .c representada
# en el lienzo
# Empieza en cero para adaptarse al numero de linea, de modo que esta se
# corresponda a la posicion en la lista
# Las lineas en blanco seran una lista vacia para ocupar el hueco correspondiente
lista_lineas_c = [[]]

# Listas con el numero de linea de las instrucciones 
# en los archivos .c y .py
prepara_c = []
prepara_py = []
lee_nodo_c = []
lee_nodo_py = []
sal_aqui_c = []
sal_aqui_py = []
sal_c = []
sal_py = []
siguiente_c = []
siguiente_py = []
lee_numero_c = []
lee_numero_py = []
luce_numero_c = []
luce_numero_py = []
error_c = []
error_py = []

# Añade un nodo al circuito
def nuevo_nodo(nuevoNodo):
    lista_nodos.append(nuevoNodo)

# Añade una arista entre nodos
def nueva_relacion(pos, nuevaRelacion):
    lista_relaciones[pos].append(nuevaRelacion)

# Crea un Desplazamiento con unas coordenadas
def cambia_desplazamiento(x, y):
    global desplazamiento
    desplazamiento = Desplazamiento(x, y)

def cambia_paso_a_paso(nuevo_paso_a_paso):
    global paso_a_paso
    paso_a_paso = nuevo_paso_a_paso
        
def cambia_fich_py(nuevo_fich_py):
    global fich_py
    fich_py = nuevo_fich_py

def nueva_linea_c(lista_coordenadas):
    lista_lineas_c.append(lista_coordenadas)

def nuevo_prepara_c(num_linea):
    prepara_c.append(num_linea)
        
def nuevo_prepara_py(num_linea):
    prepara_py.append(num_linea)
        
def nuevo_lee_nodo_c(num_linea):
    lee_nodo_c.append(num_linea)
        
def nuevo_lee_nodo_py(num_linea):
    lee_nodo_py.append(num_linea)
        
def nuevo_sal_aqui_c(num_linea):
    sal_aqui_c.append(num_linea)
        
def nuevo_sal_aqui_py(num_linea):
    sal_aqui_py.append(num_linea)
        
def nuevo_sal_c(num_linea):
    sal_c.append(num_linea)
        
def nuevo_sal_py(num_linea):
    sal_py.append(num_linea)
        
def nuevo_siguiente_c(num_linea):
    siguiente_c.append(num_linea)
        
def nuevo_siguiente_py(num_linea):
    siguiente_py.append(num_linea)
        
def nuevo_lee_numero_c(num_linea):
    lee_numero_c.append(num_linea)
        
def nuevo_lee_numero_py(num_linea):
    lee_numero_py.append(num_linea)
        
def nuevo_luce_numero_c(num_linea):
    luce_numero_c.append(num_linea)
        
def nuevo_luce_numero_py(num_linea):
    luce_numero_py.append(num_linea)
        
def nuevo_error_c(num_linea):
    error_c.append(num_linea)
        
def nuevo_error_py(num_linea):
    error_py.append(num_linea)