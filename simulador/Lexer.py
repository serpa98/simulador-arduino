# coding: utf-8
from sly import Lexer
import os
import re

CARPETA = os.path.join(r"C:\Users\Sergio\Desktop\SERGIO\Unican\4 - Cuarto\TFG" \
                       "\Compilador\grading")
FICHEROS = os.listdir(CARPETA)
TESTS = [fich for fich in FICHEROS
         if os.path.isfile(os.path.join(CARPETA, fich))
         and fich.endswith(".c")]
TESTS.sort()

class C_Lexer(Lexer):

    tokens = {AUTO,BOOL,BREAK,BYTE,CASE,CHAR,CONST,CONTINUE,DEFAULT,DO, \
              DOUBLE,ELSE,ENUM,EXTERN,FLOAT,FOR,GOTO,IF, \
              INT,LONG,REGISTER,RETURN,SHORT,SIGNED,SIZEOF,STATIC, \
              STRUCT,SWITCH,TYPEDEF,UNION,UNSIGNED,VOID,VOLATILE,WHILE, \
              DEFINE,STRING_CONST,INT_CONST,BOOL_CONST,CHAR_CONST,TYPE_ID, \
              OBJECT_ID,INCLUDE,INCLUDED_FILE,FLOAT_CONST,CONST, \
              MEMBER_SELECTION,INCREMENT,DECREMENT,LEFT_SHIFT,RIGHT_SHIFT, \
              LESS_EQUAL,GREATER_EQUAL,EQUAL_TO,NOT_EQUAL_TO,AND,OR,ASSIGN_SUM, \
              ASSIGN_DIFFERENCE,ASSIGN_PRODUCT,ASSIGN_QUOTIENT,ASSIGN_REMAINDER, \
              ASSIGN_LEFT_SHIFT,ASSIGN_RIGHT_SHIFT,ASSIGN_AND,ASSIGN_OR,ASSIGN_XOR, \
              MAIN}
    
    AUTO = r'auto'
    #BOOL = r'bool'
    BREAK = r'break'            # Sale del if o del bucle
    #BYTE = r'byte
    CASE = r'case'
    #CHAR = r'char'
    CONST = r'const'
    CONTINUE = r'continue'      # Pasa a la siguiente iteración
    DEFAULT = r'default'
    DO = r'do'
    #DOUBLE = r'double'
    ELSE = r'else'
    ENUM = r'enum'
    EXTERN = r'extern'
    #FLOAT = r'float'
    FOR = r'for'
    GOTO = r'goto'
    IF = r'if'
    #INT = r'int'
    #LONG = r'long'
    REGISTER = r'register'
    RETURN = r'return'
    #SHORT = r'short'
    SIGNED = r'signed'
    SIZEOF = r'sizeof'
    STATIC = r'static'
    STRUCT = r'struct'
    SWITCH = r'switch'
    TYPEDEF = r'typedef'
    UNION = r'union'
    UNSIGNED = r'unsigned'
    #VOID = r'void'
    VOLATILE = r'volatile'
    WHILE = r'while'

    MEMBER_SELECTION = r'->'
    INCREMENT = r'[+]{2}'
    DECREMENT = r'--'
    ASSIGN_LEFT_SHIFT = r'(<<=)'    # Especificar las reglas mas largas primero
    ASSIGN_RIGHT_SHIFT = r'(>>=)'   # >>= es mas larga que >>
    LEFT_SHIFT = r'<<'
    RIGHT_SHIFT = r'>>'
    LESS_EQUAL = r'<='
    GREATER_EQUAL = r'>='
    EQUAL_TO = r'=='
    NOT_EQUAL_TO = r'!='
    AND = r'&&'
    OR = r'\|\|'
    ASSIGN_SUM = r'[+]='
    ASSIGN_DIFFERENCE = r'-='
    ASSIGN_PRODUCT = r'[*]='
    ASSIGN_QUOTIENT = r'[/]='
    ASSIGN_REMAINDER = r'%='
    ASSIGN_AND = r'&='
    ASSIGN_OR = r'[|]='
    ASSIGN_XOR = r'\^='

    #MAIN = r'main'
    
    literals = {'[',']','(',')','{','}',',','#','*','~','.','+','-','/','%', \
                '>','<','!','&','|', \
                '^','=',';',':'}

    @_(r'#define')
    def DEFINE(self, t):
        t.type = "DEFINE"
        return t

    @_(r'#include')
    def INCLUDE(self, t):
        t.type = "INCLUDE"
        return t

    @_(r'<(.*)>')
    def INCLUDED_FILE(self, t):
        t.type = "INCLUDED_FILE"
        return t

    @_(r'"([^"])*"')
    def STRING_CONST(self, t):
        t.type = "STRING_CONST"
        """self.lineno = self.lineno + t.value.count('\n')
        t.lineno = self.lineno
        new_value = []
        for c in t.value:
            new_value.append(c)
        t.value = ''.join(new_value)"""
        return t

    @_(r"'.'")
    def CHAR_CONST(self, t):
        t.type = "CHAR_CONST"
        return t

    @_(r'\d+\.\d+')
    def FLOAT_CONST(self, t):
        t.type = "FLOAT_CONST"
        return t

    @_(r'\d+')
    def INT_CONST(self, t):
        t.type = "INT_CONST"
        return t

    @_(r'(true|false)')
    def BOOL_CONST(self, t):
        t.type = "BOOL_CONST"
        """if t.value == "true":
            t.value = True
        else:
            t.value = False"""
        return t

    @_(r'(bool|byte|char|double|float|int|long|short|void)')
    def TYPE_ID(self, t):
        t.type = "TYPE_ID"
        return t

    @_(r'([a-zA-Z1-9_]){1,31}')
    def OBJECT_ID(self, t):
        t.type = "OBJECT_ID"
        return t

    # #ifndef, #else, #endif
    @_(r'#ifndef (.*)')
    def ifndef(self, t):
        pass

    @_(r'#endif')
    def endif(self, t):
        pass

    @_(r'\/\*')
    def comentario(self, t):
        return self.begin(Comentario)

    @_(r'\*\/')
    def unmatched(self, t):
        t.type = "ERROR"
        t.value = '"Unmatched */"'
        return t

    @_(r'//.*')
    def comentario_linea(self, t):
        self.lineno = self.lineno + t.value.count('\n')
        pass

    @_(r'\t| ')
    def espacios(self, t):
        pass

    @_(r'\n+')
    def nueva_linea(self, t):
        self.lineno = self.lineno + t.value.count('\n')

    @_(r'\r+')
    def carriage_return(self, t):
        self.lineno = self.lineno + t.value.count('\r')

    def error(self, t):
        print("Illegal character '%s', linea %s" % (t.value[0], t.lineno))
        self.index = self.index + 1

    def salida(self,t):
        list_strings = []
        for token in lexer.tokenize(t):
            result = f'#{token.lineno} {token.type} '
            """if token.type == 'CONSTANT_INI':
                result += f"{str(token.value)}"""
            if token.type == 'INCLUDED_FILE':
                result += f"{str(token.value)}"
            elif token.type == 'STRING_CONST':
                result += f"{str(token.value)}"
            elif token.type == 'FLOAT_CONST':
                result += f"{str(token.value)}"
            elif token.type == 'INT_CONST':
                result += f"{str(token.value)}"
            elif token.type == 'CHAR_CONST':
                result += f"{str(token.value)}"
            elif token.type == 'BOOL_CONST':
                result += "true" if token.value else "false"
            elif token.type == 'TYPE_ID':
                result += f"{str(token.value)}"
            elif token.type == 'OBJECT_ID':
                result += f"{str(token.value)}"
            elif token.type == 'ERROR':
                result += f"{str(token.value)}"
            elif token.type in self.literals:
                result = f'#{token.lineno} \'{token.type}\' '
            else:
                result = f'#{token.lineno} {token.type}'
            list_strings.append(result)
        if t.count('/*') > t.count('*/'):
            list_strings.append(f'#{self.lineno} ERROR "EOF in comment"')
        return list_strings

lexer = C_Lexer()

class Comentario(Lexer):
    tokens = {ERROR}

    @_(r'\*\/')
    def fin_comentario(self, t):
        self.begin(C_Lexer)

    @_(r'\t| ')
    def espacios(self, t):
        pass

    @_(r'\n+')
    def nueva_linea(self, t):
        self.lineno = self.lineno + t.value.count('\n')

    @_(r'.')
    def contenido_comentario(self, t):
        pass

if __name__ == '__main__':
        #for fich in TESTS:
        lexer = C_Lexer()
        fich = "C:/Users/Sergio/Desktop/SERGIO/Unican/4 - Cuarto/TFG/Compilador/grading/1.aux.c"
        fich_entrada = open(os.path.join(CARPETA, fich), 'r')
        fich_salida = open(os.path.join(CARPETA, fich + '.out'), 'w')
        entrada = fich_entrada.read()
        texto = '\n'.join(lexer.salida(entrada))
        texto = f'#name "{fich}"\n' + texto
        fich_salida.write(texto)
        fich_entrada.close()
        fich_salida.close()
