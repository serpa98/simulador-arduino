import pygame, os, globals, sys, linecache, math
import tkinter.filedialog
from Parser import C_Parser
from Lexer import C_Lexer
from importlib import reload

# Colores utilizados 
white = (225, 225, 225, 255)
black = (0, 0, 0, 255)
blue = [5, 0, 120]
grey = [160, 160, 160]
red = [185, 0, 0]
green = [0, 102, 0]
orange = [190, 95, 0]
pink = [140, 78, 140]

color_cuadrado = orange

# Indica el nodo en el que empieza la simulacion
# Si el usuario no lo especifica sera el primero creado
primer_nodo = -1
primer_nodo_creado = False

# Establece el nuevo nodo inicial de la simulacion
def cambia_primer_nodo(nuevo_nodo):
    global primer_nodo
    primer_nodo = nuevo_nodo

# Tamaño del coche
tam_coche = 4

# Tiempos del coche
num_pasos_coche = 15
tiempo_movimiento_coche = 45
tiempo_lectura_etiqueta = 400

# Lista que guarda los colores en la parte del lienzo sobre la que se dibujara
# el coche para despues colorear esa parte como estaba.
# Los cada elemento de la lista es otra lista --> [x, y, color]
estado_circuito = []

# Archivo cargado con las instrucciones para el coche
file = ""

# Numero de nodos creados por el usuario
num_nodos = 0

# Variables cajas de texto
ancho_cajas = 90
alto_cajas = 20
separacion_horizontal_cajas = 165
separacion_vertical_cajas = 90

# Variables de los botones
tam_botones_izq = 90
tam_botones_der = 108
x_botones_izq = 10
y_botones_izq = 10
separacion_vertical_botones = 30
separacion_vertical_restaurar = 35
tam_botones_nodo_inicial = 18
tam_botones_paso_a_paso = 23
separacion_horizontal_botones_nodo_inicial = 30
separacion_vertical_botones_nodo_inicial = 30

# Coordenadas en las que se muestra el codigo del fichero
# elegido en la interfaz
x_codigo = 795
y_codigo = 35

# Otras variables
borrando_nodos = False
nodos_a_borrar = []     # Nodos seleccionados por el usuario, pueden borrarse o no
                        # (dependiendo de si se cancela la operacion o no)
posibles_nodos = ['0', '1', '2', '3', '4', '5', '6', '7']

# El tamaño de los nodos es proporcional a esta variable
dist = 6

# Coordenadas en las que se muestran los botones correspondientes
# a los nodos
x_nodo_inicial = 1175
x_nodo_inicial_aux = 1175
y_nodo_inicial = 30
y_nodo_inicial_aux = 30
lista_botones_nodo_inicial = []
coordenadas_botones_nodo_inicial = []
coordenadas_cuadros_texto = []

# Posiciones predefinidas que ocuparan los cuadros de texto
posiciones_cuadros_texto = []
x_caja = 170
x_caja_aux = 170
y_caja = 10
y_caja_aux = 10
for i in range(8):
    if i == 4:
        y_caja += separacion_vertical_cajas
        x_caja = x_caja_aux
    posiciones_cuadros_texto.append([x_caja, y_caja])
    x_caja += separacion_horizontal_cajas

# Coordenadas para dibujar los nodos
x_nodo = 15*dist
x_nodo_aux = 15*dist
y_nodo = 250
y_nodo_aux = 250
separacion_horizontal_nodos = 28*dist
separacion_vertical_nodos = 30*dist

# Lista con las cajas de texto con el numero de nodo y sus relaciones
lista_cajas = []

lista_coordenadas = []
for i in range(8):
    lista_coordenadas.append([])

# Partes del lienzo por las que iran las relaciones entre nodos
pistas_superiores = []
for i in range(6):
    pistas_superiores.append(y_nodo_aux - dist*4 - dist*i)
pistas_inferiores = []
for i in range(6):
    pistas_inferiores.append(y_nodo_aux + separacion_vertical_nodos + dist*11 + dist*i)
pistas_centrales = []
for i in range(16):
    pistas_centrales.append(y_nodo_aux + 11*dist + dist*i)

# Fuentes usadas en los botones y etiquetas
fuente_botones          = None  # Usada en los botones
fuente_info             = None  # USada para indicar los nuevos nodos, aristas y fichero
fuente_cuadros_texto    = None  # Usada en los cuadros de texto
fuente_programa         = None  # Usada para mostrar el codigo del fichero
fuente_error            = None  # Usada para mostrar los errores
fuente_etiquetas        = None  # Usada para mostrar las etiquetas de texto ("Codigo", 
                                # "Nodo inicial" y "Paso a paso")

# Zona de los mensajes de error
zona_fich = []
ancho_directorio = 360
zona_error_no_nodos = []
zona_error_no_fich = []
zona_etiquetas_texto = []
zona_error_cuadro_texto = []

# Muestra las etiquetas de texto de la interfaz
# (nodo inicia, codigo...)
def muestra_etiquetas():
    label = fuente_etiquetas.render("Código:", 1, black)                            
    globals.screen.blit(label, (x_codigo, y_nodo_inicial - 25))
    label = fuente_etiquetas.render("Nodo inicial:", 1, black)                            
    globals.screen.blit(label, (x_nodo_inicial_aux, y_nodo_inicial - 25))
    label = fuente_etiquetas.render("Paso a paso:", 1, black)
    globals.screen.blit(label, (x_nodo_inicial_aux, y_nodo_inicial + 135))

# Devuelve el valor binario de un numero en formato
# de cuatro digitos
def devuelve_binario(x):
    # Elimina el prefijo "0b" devuelto por la funcion
    binario = bin(x)[2:]
    # Coloca delante del resultado tantos ceros como
    # sea necesario para que haya cuatro digitos
    num_ceros = 4 - len(binario)
    for i in range(num_ceros):
        binario = "0" + binario
    return binario

# Dibuja la etiqueta de un nodo del circuito
def dibuja_num_nodo(numero_nodo, x, y, dist):
    # Dibuja un trozo de cinta antes del hueco en blanco
    # del primer bit (0)
    pygame.draw.line(globals.screen, black, (x,y), (x+dist,y))
    x = x + dist
    prev = None
    for num in numero_nodo:
        if num == "0":
            # Si se encuentra la secuencia 00 se dibuja una
            # linea negra entre ambos huecos en blanco
            if prev != None and prev == num:
                pygame.draw.line(globals.screen, black, (x,y), (x+dist,y))
                x = x + 2*dist
            else:
                x = x + dist
            prev = num
        else:
            # Dibuja un trozo de cinta atravesada indicando un bit 1
            pygame.draw.line(globals.screen, black, (x,y), (x+2*dist,y))
            pygame.draw.line(globals.screen, black, (x+dist,y), (x+dist,y-dist))
            pygame.draw.line(globals.screen, black, (x+dist,y), (x+dist,y+dist))
            x = x + 2*dist
            prev = num
    pygame.draw.line(globals.screen, black, (x,y), (x+dist,y))
    pygame.display.flip()
    return x

# Dibuja rectángulo con número de nodo
def dibuja_nodo(numero_nodo, x, y, dist):
    numero_binario = devuelve_binario(numero_nodo)
    x_fin = dibuja_num_nodo(numero_binario, x, y, dist)
    long = x_fin - x
    lista_coordenadas[numero_nodo].append([x_fin+10,y])
    lista_coordenadas[numero_nodo].append([x,y+long])
    pygame.draw.line(globals.screen, black, (x,y), (x-long,y))
    pygame.draw.line(globals.screen, black, (x_fin,y), (x_fin+long,y))
    pygame.draw.line(globals.screen, black, (x-long,y), (x-long,y+long))
    pygame.draw.line(globals.screen, black, (x_fin+long,y), (x_fin+long,y+long))
    pygame.draw.line(globals.screen, black, (x-long,y+long), (x_fin+long,y+long))
    pygame.display.flip()

# Dibuja los nodos creados
def dibuja_nodos():
    global x_nodo
    global y_nodo
    global x_nodo_aux
    global y_nodo_aux
    #global num_nodos
    num_nodos = 0
    for nodo in globals.lista_nodos:
        if num_nodos == 4:
            x_nodo = x_nodo_aux
            y_nodo = y_nodo + separacion_vertical_nodos
        dibuja_nodo(int(nodo), x_nodo, y_nodo, dist)
        num_nodos = num_nodos + 1
        x_nodo = x_nodo + separacion_horizontal_nodos
    #num_nodos = 0
    x_nodo = x_nodo_aux
    y_nodo = y_nodo_aux

# Dibuja relaciones entre nodos
def dibuja_relaciones():
    lista1 = globals.lista_nodos[0:4]
    lista2 = globals.lista_nodos[4:8]
    k = 0
    l = 0
    n = 0
    # Cambiar k, l y n por listas, de modo que cada nodo tenga sus propias k, n y l
    k_lista = [0] * 8
    l_lista = [0] * 8
    n_lista = [0] * 8
    for i, rel in enumerate(globals.lista_relaciones):
        for nodo in rel:
            if lista_coordenadas[i] != []:
                # Si ambos nodos están arriba
                if str(i) in lista1 and nodo in lista1:
                    x_inicio = lista_coordenadas[i][0][0] + dist*k_lista[i] + dist # Este dist es para evitar que la simulacion empiece en la arista
                    y_inicio = lista_coordenadas[i][0][1]
                    x_fin = lista_coordenadas[int(nodo)][0][0] + dist*k_lista[int(nodo)] + dist
                    y_fin = lista_coordenadas[int(nodo)][0][1]
                    pygame.draw.line(globals.screen, black, (x_inicio, y_inicio), (x_inicio, pistas_superiores[k]))
                    pygame.draw.line(globals.screen, black, (x_inicio, pistas_superiores[k]), (x_fin, pistas_superiores[k]))
                    pygame.draw.line(globals.screen, black, (x_fin, pistas_superiores[k]), (x_fin, y_fin))
                    k_lista[i] = k_lista[i] + 1
                    k_lista[int(nodo)] = k_lista[int(nodo)] + 1
                    k = k + 1
                # Si uno está arriba y otro abajo
                if str(i) in lista1 and nodo in lista2:
                    x_inicio = lista_coordenadas[i][1][0] + dist*l_lista[i] + dist
                    y_inicio = lista_coordenadas[i][1][1]
                    x_fin = lista_coordenadas[int(nodo)][0][0] + dist*l_lista[int(nodo)] + dist
                    y_fin = lista_coordenadas[int(nodo)][0][1]
                    pygame.draw.line(globals.screen, black, (x_inicio, y_inicio), (x_inicio, pistas_centrales[l]))
                    pygame.draw.line(globals.screen, black, (x_inicio, pistas_centrales[l]), (x_fin, pistas_centrales[l]))
                    pygame.draw.line(globals.screen, black, (x_fin, pistas_centrales[l]), (x_fin, y_fin))
                    l_lista[i] = l_lista[i] + 1
                    l_lista[int(nodo)] = l_lista[int(nodo)] + 1
                    l = l + 1
                # Si uno está abajo y otro arriba
                if str(i) in lista2 and nodo in lista1:
                    x_inicio = lista_coordenadas[i][0][0] + dist*l_lista[i] + dist
                    y_inicio = lista_coordenadas[i][0][1]
                    x_fin = lista_coordenadas[int(nodo)][1][0] + dist*l_lista[int(nodo)] + dist
                    y_fin = lista_coordenadas[int(nodo)][1][1]
                    pygame.draw.line(globals.screen, black, (x_inicio, y_inicio), (x_inicio, pistas_centrales[l]))
                    pygame.draw.line(globals.screen, black, (x_inicio, pistas_centrales[l]), (x_fin, pistas_centrales[l]))
                    pygame.draw.line(globals.screen, black, (x_fin, pistas_centrales[l]), (x_fin, y_fin))
                    l_lista[i] = l_lista[i] + 1
                    l_lista[int(nodo)] = l_lista[int(nodo)] + 1
                    l = l + 1
                # Si ambos están abajo
                elif str(i) in lista2 and nodo in lista2:
                    x_inicio = lista_coordenadas[i][1][0] + dist*n_lista[i]
                    y_inicio = lista_coordenadas[i][1][1]
                    x_fin = lista_coordenadas[int(nodo)][1][0] + dist*n_lista[int(nodo)]
                    y_fin = lista_coordenadas[int(nodo)][1][1]
                    pygame.draw.line(globals.screen, black, (x_inicio, y_inicio), (x_inicio, pistas_inferiores[n]))
                    pygame.draw.line(globals.screen, black, (x_inicio, pistas_inferiores[n]), (x_fin, pistas_inferiores[n]))
                    pygame.draw.line(globals.screen, black, (x_fin, pistas_inferiores[n]), (x_fin, y_fin))
                    n_lista[i] = n_lista[i] + 1
                    n_lista[int(nodo)] = n_lista[int(nodo)] + 1
                    n = n + 1

# Recorre el fichero .c para guardar las lineas donde
# estan las instrucciones del simulador
def recorre_fichero_c(entrada):
    # Borra los datos del fichero anterior
    globals.lista_lineas_c[:] = [[]]
    globals.prepara_c[:] = []
    globals.lee_nodo_c[:] = []
    globals.sal_aqui_c[:] = []
    globals.sal_c[:] = []
    globals.siguiente_c[:] = []
    globals.lee_numero_c[:] = []
    globals.luce_numero_c[:] = []
    globals.error_c[:] = []

    lines = entrada.splitlines()
    y = y_codigo
    i = 1
    separador = "__________________________________________________________"
    label = fuente_programa.render(separador, 1, black)
    globals.screen.blit(label, (x_codigo, y))
    # Controla el numero de tabulados de las lineas del fichero,
    # para añadir mas en caso de ser necesario
    num_espacios = 0
    num_espacios_establecido = False
    y = y + 16
    # Recorre las lineas del fichero
    for etiq in lines:
        escribir_linea = False
        tab_linea = 0
        # Recorre la linea para ver su contenido: si solo contiene
        # espacios en blanco o una llave de cierre "}" no se muestra
        for c in etiq:
            if ord(c) != 32 and ord(c) != 125:
                escribir_linea = True
                if not num_espacios_establecido:
                    num_espacios += 1
                break
            tab_linea += 1
        if escribir_linea:
            # Detectar las lineas correspondientes a funciones para 
            # dejar una linea en blanco
            if (("bool" in etiq or "byte" in etiq or "char" in etiq or \
                "double" in etiq or "float" in etiq or "int" in etiq or \
                "long" in etiq or "short" in etiq or "void" in etiq) and \
                "(" in etiq and ")" in etiq and "{" in etiq):
                y = y + 5
            # Tabulado necesario dependiendo del ancho del tabulado
            # del fichero original
            tab = math.ceil(4 / num_espacios)
            tab = tab * tab_linea
            etiq = tab * " " + etiq
            label = fuente_programa.render(etiq, 1, black)
            label_size = label.get_size()
            # Si la linea no cabe en el espacio asignado
            # se incluyen solo los caracteres que quepan
            if label_size[0] > (ancho_directorio - 25):
                while label_size[0] > (ancho_directorio - 25):
                    etiq = etiq[0:len(etiq) - 1]
                    label = fuente_programa.render(etiq, 1, black)
                    label_size = label.get_size()
                etiq = etiq + " ..."
                label = fuente_programa.render(etiq, 1, black)
            globals.screen.blit(label, (x_codigo, y))
            globals.nueva_linea_c([x_codigo, y])
            y = y + 13
            if "prepara" in etiq:
                globals.nuevo_prepara_c(i)
            elif "lee_nodo" in etiq:
                globals.nuevo_lee_nodo_c(i)
            elif "sal_aqui" in etiq:
                globals.nuevo_sal_aqui_c(i)
            elif "sal" in etiq:
                globals.nuevo_sal_c(i)
            elif "siguiente" in etiq:
                globals.nuevo_siguiente_c(i)
            elif "lee_numero" in etiq:
                globals.nuevo_lee_numero_c(i)
            elif "luce_numero" in etiq:
                globals.nuevo_luce_numero_c(i)
            elif "error" in etiq:
                globals.nuevo_error_c(i)
        else:
            globals.nueva_linea_c([])
        i = i + 1

# Recorre el fichero .py para guardar las lineas donde
# estan las instrucciones del simulador
def recorre_fichero_py(entrada):
    lines = entrada.splitlines()
    i = 1
    for etiq in lines:
        if "prepara" in etiq:
            globals.nuevo_prepara_py(i)
        elif "lee_nodo" in etiq:
            globals.nuevo_lee_nodo_py(i)
        elif "sal_aqui" in etiq:
            globals.nuevo_sal_aqui_py(i)
        elif "sal" in etiq:
            globals.nuevo_sal_py(i)
        elif "siguiente" in etiq:
            globals.nuevo_siguiente_py(i)
        elif "lee_numero" in etiq:
            globals.nuevo_lee_numero_py(i)
        elif "luce_numero" in etiq:
            globals.nuevo_luce_numero_py(i)
        elif "error" in etiq:
            globals.nuevo_error_py(i)
        i = i + 1

    # Quitar los dos primeros elementos de las listas de posiciones
    # los cuales se corresponden a {def inst(): desp.inst()}
    globals.prepara_py = globals.prepara_py[2:len(globals.prepara_py)]
    globals.lee_nodo_py = globals.lee_nodo_py[2:len(globals.lee_nodo_py)]
    globals.sal_aqui_py = globals.sal_aqui_py[2:len(globals.sal_aqui_py)]
    globals.sal_py = globals.sal_py[2:len(globals.sal_py)]
    globals.siguiente_py = globals.siguiente_py[2:len(globals.siguiente_py)]
    globals.lee_numero_py = globals.lee_numero_py[2:len(globals.lee_numero_py)]
    globals.luce_numero_py = globals.luce_numero_py[2:len(globals.luce_numero_py)]
    globals.error_py = globals.error_py[2:len(globals.error_py)]

# Clase que muestra el recorrido por el grafo
class Desplazamiento:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        # Coordenadas correspondientes al cuadrado rojo que indica
        # en que parte del programa se encuentra la ejecucion
        self.x_linea = 0
        self.y_linea = 0
        self.direccion = 'der'  # arr, ab, izq, der

    """ Dibuja el cuadrado que representa el coche moviendose por el circuito """
    def dibuja_cuadrado(self):
        global estado_circuito
        estado_circuito = []
        x = self.x - tam_coche
        y = self.y - tam_coche
        aux = 0
        for i in range(pow(2*tam_coche, 2)):
            if aux == 2*tam_coche:
                x = self.x - tam_coche
                y = y + 1
                aux = 0
            estado_circuito.append([x, y, globals.screen.get_at((x, y))])
            x = x + 1
            aux = aux + 1
        pygame.draw.rect(globals.screen, color_cuadrado, [self.x-tam_coche, self.y-tam_coche, 2*tam_coche, 2*tam_coche])
        pygame.display.flip()

    """ Borra el coche, dejando el lienzo como estaba antes"""
    def borra_cuadrado(self):
        for i in estado_circuito:
            x = i[0]
            y = i[1]
            color = i[2]
            globals.screen.set_at((x, y), color)
        
    """ Configuracion inicial (devuelve uno en esta implementacion) """
    def prepara(self):
        return 1

    """ Recorre un nodo devolviendo su etiqueta, su grado y el número de orden
        de la arista previa al punto de arranque (si comienza tras la etiqueta
        vale cero) """
    def lee_nodo(self, nodo, grado, anterior):
        aristas_hasta_etiqueta = 1
        while self.siguiente():
            aristas_hasta_etiqueta = aristas_hasta_etiqueta + 1
        # Retrocede hasta la zona sin cinta de la etiqueta para que lee_numero()
        # interprete los cuatro bits (el primer cero y los otros tres bits)
        self.x = self.x - 1
        while globals.screen.get_at((self.x,self.y)) != white:
            self.x = self.x - 1
        self.x = self.x - 1
        ###
        nodo = self.lee_numero()
        grado = 0
        while self.siguiente():
            grado = grado + 1
        anterior = grado - aristas_hasta_etiqueta + 1
        ### Retrocede hasta la zona sin cinta de la etiqueta
        self.x = self.x - 1
        while globals.screen.get_at((self.x,self.y)) != white:
            self.x = self.x - 1
        self.x = self.x - 1
        self.lee_numero_alt()
        print("Etiqueta: ",nodo,"; Grado: ",grado,"; Anterior ",anterior)
        return nodo, grado, anterior

    """ Toma la arista recién encontrada """
    def sal_aqui(self):
        if self.direccion == 'der':
            self.direccion = 'arr'
        elif self.direccion == 'ab':
            self.direccion = 'der'
        elif self.direccion == 'izq':
            self.direccion = 'ab'
        else:
            self.direccion = 'izq'
        self.siguiente_nodo()

    """ Toma la arista indicada """
    def sal(self, arista):
        self.lee_numero()
        for i in range(arista):
            self.siguiente()
        self.sal_aqui()

    """ Metodo alternativo a siguiente(), que lleva al coche al nodo correspondiente """
    def siguiente_nodo(self):
        cont = 0
        self.borra_cuadrado()
        while True:
            if cont == num_pasos_coche:
                self.dibuja_cuadrado()
                pygame.time.delay(tiempo_movimiento_coche)
                self.borra_cuadrado()
                cont = 0
            cont += 1
            # si va hacia la derecha:
            if self.direccion == 'der':
                self.x = self.x + 1
                # ir arriba si la union va arriba
                if globals.screen.get_at((self.x,self.y-1)) != white and\
                    globals.screen.get_at((self.x,self.y+1)) == white:
                    self.direccion = 'arr'
                # ir abajo si la union va abajo
                elif globals.screen.get_at((self.x,self.y+1)) != white and\
                    globals.screen.get_at((self.x,self.y-1)) == white:
                    self.direccion = 'ab'
            # si va hacia arriba:
            if self.direccion == 'arr':
                self.y = self.y - 1
                # ir a la izq si la union va a la izq
                if globals.screen.get_at((self.x-1,self.y)) != white and\
                    globals.screen.get_at((self.x+1,self.y)) == white:
                    self.direccion = 'izq'
                # ir a la der si la union va a la der
                if globals.screen.get_at((self.x+1,self.y)) != white and\
                    globals.screen.get_at((self.x-1,self.y)) == white:
                    self.direccion = 'der'
                # ir a la izq si se llega al nodo
                if globals.screen.get_at((self.x+1,self.y)) != white and\
                    globals.screen.get_at((self.x-1,self.y)) != white and\
                    globals.screen.get_at((self.x,self.y-1)) == white:
                    self.direccion = 'izq'
                    #self.dibuja_cuadrado()
                    return
            # si va hacia la izq:
            if self.direccion == 'izq':
                self.x = self.x - 1
                # ir arriba si la union va arriba
                if globals.screen.get_at((self.x,self.y-1)) != white and\
                    globals.screen.get_at((self.x,self.y+1)) == white:
                    self.direccion = 'arr'
                # ir abajo si la union va abajo
                elif globals.screen.get_at((self.x,self.y+1)) != white and\
                    globals.screen.get_at((self.x,self.y-1)) == white:
                    self.direccion = 'ab'
            # si va hacia abajo:
            if self.direccion == 'ab':
                self.y = self.y + 1
                # ir a la der si la union va a la der
                if globals.screen.get_at((self.x+1,self.y)) != white and\
                    globals.screen.get_at((self.x-1,self.y)) == white:
                    self.direccion = 'der'
                # ir a la izq si la union va a la izq
                if globals.screen.get_at((self.x-1,self.y)) != white and\
                    globals.screen.get_at((self.x+1,self.y)) == white:
                    self.direccion = 'izq'
                # ir a la der si se llega al nodo
                if globals.screen.get_at((self.x+1,self.y)) != white and\
                    globals.screen.get_at((self.x-1,self.y)) != white and\
                    globals.screen.get_at((self.x,self.y+1)) == white:
                    self.direccion = 'der'
                    return

    """ Avanza por el camino hasta encontrar un hueco (devuelve False) o
        un trozo de cinta atravesada (devuelve True) """
    def siguiente(self):
        cont = 0
        self.borra_cuadrado()
        while True:
            if cont == num_pasos_coche:
                self.dibuja_cuadrado()
                pygame.time.delay(tiempo_movimiento_coche)
                self.borra_cuadrado()
                cont = 0
            cont += 1
            if self.direccion == 'der':
                self.x = self.x + 1
                # Si el pixel es blanco
                if globals.screen.get_at((self.x,self.y)) == white:
                    # Si el pixel es blanco es que se ha llegado al extremo del circuito,
                    # si no es que está en el número del nodo
                    if globals.screen.get_at((self.x+dist+1,self.y)) == white:
                        self.x = self.x - 1
                        self.direccion = 'ab'
                    else:
                        while globals.screen.get_at((self.x,self.y)) == white:
                            self.x = self.x + 1
                        return False
                # Hay una arista hacia arriba
                if globals.screen.get_at((self.x,self.y-1)) != white:
                    return True
            elif self.direccion == 'ab':
                self.y = self.y + 1
                # Si el pixel es blanco
                if globals.screen.get_at((self.x,self.y)) == white:
                    self.y = self.y - 1
                    self.direccion = 'izq'
                # Hay una arista hacia la derecha
                if globals.screen.get_at((self.x+1,self.y)) != white:
                    return True
            elif self.direccion == 'izq':
                self.x = self.x - 1
                # Si el pixel es blanco
                if globals.screen.get_at((self.x,self.y)) == white:
                    self.x = self.x + 1
                    self.direccion = 'arr'
                # Hay una arista hacia abajo
                if globals.screen.get_at((self.x,self.y+1)) != white:
                    return True
            else:
                self.y = self.y - 1
                # Si el pixel es blanco
                if globals.screen.get_at((self.x,self.y)) == white:
                    self.y = self.y + 1
                    self.direccion = 'der'
                # Hay una arista hacia la izquierda
                if globals.screen.get_at((self.x-1,self.y)) != white:
                    return True

    """ Lee una etiqueta """
    def lee_numero(self):
        while self.siguiente():
            continue
        resultado = '0'
        for i in range(3):
            siguiente = self.siguiente()
            self.dibuja_cuadrado()
            pygame.time.delay(tiempo_lectura_etiqueta)
            self.borra_cuadrado()
            if siguiente:
                resultado = resultado + '1'
            else:
                resultado = resultado + '0'
        return int(resultado,2)

    """ Funcion alternativa a lee_numero, para llamarla al terminar lee_nodo """
    def lee_numero_alt(self):
        while self.siguiente():
            continue
        for i in range(3):
            self.dibuja_cuadrado()
            pygame.time.delay(tiempo_movimiento_coche)
            self.borra_cuadrado()
            self.siguiente()

    """ Muestra un numero """
    def luce_numero(self, num):
        print("Luce numero: ", num)

    """ Señaliza un acontecimiento inesperado """
    def error(self):
        print("Funcion error --> Fallo inesperado en la simulación")

    """ Espera hasta que el usuario ejecute el siguiente paso del programa """
    def espera(self, linea):
        linea_anterior = linecache.getline(globals.fich_py, linea - 1)
        pos_py = -1
        linea_c = -1
        # Se buscan las coordenadas de la instruccion correspondiente
        # para indicar en la interfaz en que parte de la ejecucion se
        # encuentra el programa
        if "prepara" in linea_anterior:
            pos_py = globals.prepara_py.index(linea - 1)
            linea_c = globals.prepara_c[pos_py]
        elif "lee_nodo" in linea_anterior:
            pos_py = globals.lee_nodo_py.index(linea - 1)
            linea_c = globals.lee_nodo_c[pos_py]
        elif "sal_aqui" in linea_anterior:
            pos_py = globals.sal_aqui_py.index(linea - 1)
            linea_c = globals.sal_aqui_c[pos_py]
        elif "sal" in linea_anterior:
            pos_py = globals.sal_py.index(linea - 1)
            linea_c = globals.sal_c[pos_py]
        elif "siguiente" in linea_anterior:
            pos_py = globals.siguiente_py.index(linea - 1)
            linea_c = globals.siguiente_c[pos_py]
        elif "lee_numero" in linea_anterior:
            pos_py = globals.lee_numero_py.index(linea - 1)
            linea_c = globals.lee_numero_c[pos_py]
        elif "luce_numero" in linea_anterior:
            pos_py = globals.luce_numero_py.index(linea - 1)
            linea_c = globals.luce_numero_c[pos_py]
        elif "error" in linea_anterior:
            pos_py = globals.error_py.index(linea - 1)
            linea_c = globals.error_c[pos_py]
        # pintar la linea en posicion linea c
        pygame.draw.rect(globals.screen, white, [self.x_linea - 10, self.y_linea + 7, 5, 5])
        self.x_linea = globals.lista_lineas_c[linea_c][0]
        self.y_linea = globals.lista_lineas_c[linea_c][1]
        pygame.draw.rect(globals.screen, red, [self.x_linea - 10, self.y_linea + 7, 5, 5])

        # Si la ejecucion paso a paso esta activada se espera hasta
        # que se pulse el boton "Siguiente paso"
        if globals.paso_a_paso:
            espera = True
            self.dibuja_cuadrado()
            while espera:
                for event in pygame.event.get():
                    x, y = pygame.mouse.get_pos()
                    if event.type == pygame.QUIT:
                        pygame.quit()
                    if event.type == pygame.MOUSEBUTTONDOWN:
                        if pygame.mouse.get_pressed()[0]:
                            if globals.boton_siguiente_paso.rect.collidepoint(x, y):
                                espera = False
                                self.borra_cuadrado()

# Clase botón, recibe los parametros texto, coordenadas 
# (x e y) y color
class Boton:
    def __init__(self, text, x, y, color):
        self.texto = text
        self.x = x
        self.y = y
        self.color = color
        self.diseno_boton(color)
 
    """ Establece las dimensiones del boton y su superficie 
        con el color correspondiente """
    def diseno_boton(self, color):
        label = fuente_botones.render(self.texto, 1, white)
        self.size = label.get_size()
        self.size = list(self.size)
        if self.texto != " Sig. paso ":
            self.size[0] = tam_botones_izq
        if self.texto == " Borrar nodos" or self.texto == "Cancelar":
            self.size[0] = tam_botones_der
        self.surface = pygame.Surface(self.size)
        self.surface.fill(color)
        self.surface.blit(label, (0, 0))
        self.rect = pygame.Rect(self.x, self.y, self.size[0], self.size[1])

    """ Muestra el boton en la interfaz """
    def muestra_boton(self, boton):
        if boton == globals.boton_siguiente_paso:
            if globals.paso_a_paso:
                globals.screen.blit(self.surface, (self.x, self.y))
            else:
                pygame.draw.rect(globals.screen, white, pygame.Rect(self.x, self.y, self.size[0], self.size[1]))
        else:
            globals.screen.blit(self.surface, (self.x, self.y))
 
    """ Anade un nuevo cuadro de texto a la interfaz
        para crear nuevos nodos, en caso de que se haya
        pulsado el boton """
    def boton_click(self, event, nuevo_nodo, num_nodo):
        if num_nodos < 8:
            x, y = pygame.mouse.get_pos()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if pygame.mouse.get_pressed()[0]:
                    if globals.boton_nodo.rect.collidepoint(x, y):
                        if len(zona_error_no_nodos) > 0:
                            z = zona_error_no_nodos
                            pygame.draw.rect(globals.screen, white, pygame.Rect(z[0], z[1], z[2], z[3]))
                            pygame.draw.rect(globals.screen, white, pygame.Rect(z[4], z[5], z[6], z[7]))
                            zona_error_no_nodos[:] = []
                            pygame.draw.rect(globals.screen, white, pygame.Rect(globals.boton_restaurar.x, globals.boton_restaurar.y - 10, tam_botones_izq, 35))
                            globals.boton_restaurar.y -= 40
                            label = fuente_programa.render("_______________", 1, black)                            
                            globals.screen.blit(label, (9, 85))
                        self.cuadro_texto(nuevo_nodo, num_nodo)

    """ Crea un nuevo cuadro de texto directamente
    Esta funcion puede ser llamada al pulsar el boton "Nuevo nodo"
    o al crear un nuevo nodo desde el cuadro de texto "Arista" """
    def cuadro_texto(self, nuevo_nodo, num_nodo):
        # nuevo_nodo indica si hay que crear un nodo nuevo debido a una
        # relación con un nodo que aún no existe
        x = posiciones_cuadros_texto[num_nodos][0]
        y = posiciones_cuadros_texto[num_nodos][1]
        nodo = Nuevo_nodo(x, y, nuevo_nodo, num_nodo)

    """ Muestra un mensaje de error junto al boton "Simulacion" """
    def error_simulacion(self, lista, mensaje):
        pygame.draw.rect(globals.screen, white, pygame.Rect(globals.boton_restaurar.x, globals.boton_restaurar.y - 10, tam_botones_izq, 35))
        globals.boton_restaurar.y += 40
        if len(lista_botones_nodo_inicial) > 0 or file != '' or globals.paso_a_paso:
            label = fuente_programa.render("_______________", 1, black)                            
            globals.screen.blit(label, (x_botones_izq - 1, globals.boton_simulacion.y + separacion_vertical_botones + 35))
        label = fuente_error.render("ERROR:", 1, red)
        tam_label = label.get_size()
        lista.extend((x_botones_izq, globals.boton_simulacion.y + separacion_vertical_botones, tam_label[0], tam_label[1]))
        globals.screen.blit(label, (x_botones_izq, globals.boton_simulacion.y + separacion_vertical_botones))
        label = fuente_error.render(mensaje, 1, red)
        tam_label = label.get_size()
        lista.extend((x_botones_izq, globals.boton_simulacion.y + separacion_vertical_botones + 20, tam_label[0], tam_label[1]))
        globals.screen.blit(label, (x_botones_izq, globals.boton_simulacion.y + separacion_vertical_botones + 20))

    """ Se ejecuta la simulacion """
    def boton_terminar(self, event):
        x, y = pygame.mouse.get_pos()
        if event.type == pygame.MOUSEBUTTONDOWN:
            if pygame.mouse.get_pressed()[0]:
                if globals.boton_simulacion.rect.collidepoint(x, y):
                    try:
                        if globals.lista_nodos == []:
                            if len(zona_error_no_nodos) == 0:
                                self.error_simulacion(zona_error_no_nodos, "Circuito sin nodos")
                            raise Exception("Ningun nodo en el circuito")
                        elif file == "":
                            if len(zona_error_no_fich) == 0:
                                self.error_simulacion(zona_error_no_fich, "Fichero no elegido")
                            raise Exception("Ningun archivo seleccionado")

                        # Pintar el circuito de blanco para borrar el circuito anterior
                        pygame.draw.rect(globals.screen, white, pygame.Rect(5, 195, 790, 210))

                        # Pintar la zona del codigo en caso de que se cambie de fichero
                        pygame.draw.rect(globals.screen, white, pygame.Rect(x_codigo, y_codigo + 16, ancho_directorio, 700))

                        # Dibuja el circuito
                        dibuja_nodos()
                        dibuja_relaciones()

                        # Dibuja el boton de siguiente paso
                        if globals.paso_a_paso:
                            globals.boton_siguiente_paso.muestra_boton(globals.boton_siguiente_paso)

                        # Crear el desplazamiento dependiendo del nodo inicial elegido
                        global primer_nodo
                        x = lista_coordenadas[primer_nodo][0][0]
                        y = lista_coordenadas[primer_nodo][0][1]
                        globals.cambia_desplazamiento(x, y)

                        # Cambia las barras de la direccion de "\" a "/"
                        fich_c = file
                        fich_c = rf'{fich_c}'
                        extension = ""
                        for c in reversed(fich_c):
                            extension = c + extension
                            if c == ".":
                                break
                        fich_py = fich_c[0:len(fich_c)-len(extension)] + ".py"
                        globals.cambia_fich_py(fich_py)
                        
                        # Uso del lexer y parser para generar el fichero .py
                        fich_entrada = open(fich_c, 'r')
                        fich_salida = open(fich_py, 'w')
                        print()
                        print("Fichero: ", fich_entrada)
                        entrada = fich_entrada.read()
                        recorre_fichero_c(entrada)
                        
                        # Creacion de lexer y parser para compilar el fichero
                        lexer = C_Lexer()
                        parser = C_Parser()
                        texto = parser.parse(lexer.tokenize(entrada))
                        resultado = '\n'.join([c for c in texto.str(0).split('\n')])

                        # Incluir las funciones del circuito en el fichero python
                        dir_path = os.path.dirname(os.path.realpath(__file__))
                        dir_path = dir_path.replace("\\", "/")
                        get_linea = f'inspect.currentframe().f_back.f_lineno'
                        import_directory = f"import sys\nsys.path.insert(1, '{dir_path}')\n\n"
                        import_circuito = f"from Circuito import *\nfrom globals import desplazamiento\n\n"
                        import_inspect = f'import inspect\n\n'
                        funcion_prepara = f'def prepara():\n\tdesplazamiento.prepara()'
                        funcion_lee_nodo = f'def lee_nodo(nodo, grado, anterior):\n\treturn desplazamiento.lee_nodo(nodo, grado, anterior)'
                        funcion_sal_aqui = f'def sal_aqui():\n\tdesplazamiento.sal_aqui()'
                        funcion_sal = f'def sal(arista):\n\tdesplazamiento.sal(arista)'
                        funcion_siguiente = f'def siguiente():\n\treturn desplazamiento.siguiente()'
                        funcion_lee_numero = f'def lee_numero():\n\tdesplazamiento.lee_numero()'
                        funcion_luce_numero = f'def luce_numero(num):\n\tdesplazamiento.luce_numero(num)'
                        funcion_error = f'def error():\n\tdesplazamiento.error()'
                        funcion_espera = f'def espera():\n\tdesplazamiento.espera({get_linea})'
                        lista_funciones = f'{funcion_prepara}\n\n{funcion_lee_nodo}\n\n{funcion_sal_aqui}\n\n{funcion_sal}\n\n'
                        lista_funciones = f'{lista_funciones}{funcion_siguiente}\n\n{funcion_lee_numero}\n\n{funcion_luce_numero}\n\n'
                        lista_funciones = f'{lista_funciones}{funcion_error}\n\n{funcion_espera}\n\n'
                        resultado = f"{import_directory}{import_circuito}{import_inspect}{lista_funciones}{resultado}"
                        resultado = f"{resultado}\n\nsetup()\n\n"
                        #resultado = f"{resultado}while True:\n\tloop()"
                        recorre_fichero_py(resultado)

                        # Escribir en el fichero y cerrar los archivos
                        fich_salida.write(resultado)
                        fich_entrada.close()
                        fich_salida.close()

                        # Conseguir el directorio y el modulo del fichero parseado para
                        # ejecutar las funciones setup y loop
                        modulo_py = ""
                        directorio_py = ""
                        comienzo_directorio = False
                        for c in reversed(fich_py):
                            if not comienzo_directorio:
                                if c == "/":
                                    comienzo_directorio = True
                                    continue
                                modulo_py = c + modulo_py
                            else:
                                directorio_py = c + directorio_py
                        modulo_py = modulo_py[0:len(modulo_py)-3]
                        sys.path.insert(1, directorio_py)
                        if modulo_py not in sys.modules.keys():
                            exec(f'import {modulo_py}')
                        else:
                            exec(f'reload({modulo_py})')
                            """print(sys.modules.keys())
                            exec(f'del {modulo_py}')
                            exec(f'import {modulo_py}')"""

                        # Borrar el boton "Sig. paso" de la interfaz hasta
                        # la proxima simulacion
                        b = globals.boton_siguiente_paso
                        pygame.draw.rect(globals.screen, white, pygame.Rect(b.x, b.y, b.size[0], b.size[1]))

                        print("Fichero completado")
                        
                    except Exception as e:
                        exc_type, exc_obj, exc_tb = sys.exc_info()
                        print(f"Excepcion, linea {exc_tb.tb_lineno}: {e}")

    """ Establece el fichero elegido por el usuario """
    def get_file(self, event):
        x, y = pygame.mouse.get_pos()
        if event.type == pygame.MOUSEBUTTONDOWN:
            if pygame.mouse.get_pressed()[0]:
                if globals.boton_fichero.rect.collidepoint(x, y):
                    if len(zona_error_no_fich) > 0:
                        z = zona_error_no_fich
                        pygame.draw.rect(globals.screen, white, pygame.Rect(z[0], z[1], z[2], z[3]))
                        pygame.draw.rect(globals.screen, white, pygame.Rect(z[4], z[5], z[6], z[7]))
                        zona_error_no_fich[:] = []
                        pygame.draw.rect(globals.screen, white, pygame.Rect(globals.boton_restaurar.x, globals.boton_restaurar.y - 10, tam_botones_izq, 35))
                        globals.boton_restaurar.y -= 40
                        label = fuente_programa.render("_______________", 1, black)                            
                        globals.screen.blit(label, (9, 85))
                    try:
                        root = tkinter.Tk()
                        root.withdraw()
                        fich = tkinter.filedialog.askopenfilename(filetypes = [('Archivos Arduino', '.c .ino')])
                        if fich != '':
                            pygame.draw.rect(globals.screen, white, pygame.Rect(x_codigo, 26, ancho_directorio, 46))
                            global file
                            file = fich
                            print("Fichero seleccionado: ", file)
                            direccion_cabe = True
                            # Comprueba si la direccion cabe en el espacio asignado,
                            # en caso contrario no se incluye la direccion entera
                            while True:
                                label = fuente_info.render(fich, 1, black)
                                label_size = label.get_size()
                                if label_size[0] > (ancho_directorio - 15):
                                    direccion_cabe = False
                                    for c in fich:
                                        if c == "/":
                                            fich = fich[1:]
                                            break
                                        fich = fich[1:]
                                else:
                                    break
                            if not direccion_cabe:
                                fich = f"../{fich}"
                            label = fuente_info.render(fich, 1, black)
                            globals.screen.blit(label, (x_codigo, y_codigo - 13))
                    except Exception as e:
                        exc_type, exc_obj, exc_tb = sys.exc_info()
                        print(f"Excepcion, linea {exc_tb.tb_lineno}: {e}")

    """ Elimina todos los cambios realizados por el usuario en la simulacion """
    def restaurar_datos(self, event):
        x, y = pygame.mouse.get_pos()
        if event.type == pygame.MOUSEBUTTONDOWN:
            if pygame.mouse.get_pressed()[0]:
                if globals.boton_restaurar.rect.collidepoint(x, y):
                    globals.lista_nodos[:] = []
                    globals.lista_relaciones[:] = []
                    for i in range(8):
                        globals.lista_relaciones.append([])
                    lista_cajas[:] = []
                    lista_botones_nodo_inicial[:] = []
                    global x_nodo_inicial
                    global y_nodo_inicial
                    global primer_nodo
                    global primer_nodo_creado
                    global file
                    x_nodo_inicial = x_nodo_inicial_aux
                    y_nodo_inicial = y_nodo_inicial_aux
                    # Eliminar el fichero
                    file = ""
                    global num_nodos
                    num_nodos = 0
                    primer_nodo = -1
                    primer_nodo_creado = False
                    # Restaurar ejecucion paso a paso
                    if globals.paso_a_paso:
                        globals.boton_no.no_paso_a_paso()
                    globals.screen.fill(white)
                    muestra_etiquetas()
                    print("Restaurar")

    """ Elimina los nodos elegidos por el usuario """
    def borrar_nodos(self, event):
        # Comprueba si hay nodos en el circuito antes de empezar el borrado
        if len(lista_botones_nodo_inicial) > 0:
            x, y = pygame.mouse.get_pos()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if pygame.mouse.get_pressed()[0]:
                    if globals.boton_borrar_nodos.rect.collidepoint(x, y):
                        global borrando_nodos
                        global primer_nodo
                        global primer_nodo_creado
                        # Comienza el proceso de borrado al pulsar el boton
                        if not borrando_nodos:
                            borrando_nodos = True
                            globals.boton_borrar_nodos.diseno_boton(red)
                            for boton in lista_botones_nodo_inicial:
                                boton.estado = 2
                                boton.diseno_boton(boton.colores[boton.estado])
                        else:
                            borrando_nodos = False
                            globals.boton_borrar_nodos.diseno_boton(blue)
                            b = globals.boton_cancelar
                            pygame.draw.rect(globals.screen, white, pygame.Rect(b.x, b.y, b.size[0], b.size[1]))
                            # Guardar el indice para ver las posiciones que se han borrado
                            # y asi desplazar despues los botones que queden a los huecos dejados
                            # por los botones borrados
                            nodos_borrados = []
                            for i, boton in enumerate(lista_botones_nodo_inicial):
                                # Busca los nodos que haya que borrar
                                for n in nodos_a_borrar:
                                    if boton.texto == n:
                                        pygame.draw.rect(globals.screen, white, pygame.Rect(boton.x, boton.y, boton.size[0], boton.size[1]))
                                        nodos_borrados.append(boton)
                            # Borrar los botones y los nodos
                            cajas_a_borrar = []
                            for boton in nodos_borrados:
                                # Pintar sobre los cuadros de texto y etiquetas de los nodos borrados
                                i = globals.lista_nodos.index(boton.texto)
                                cuadro = coordenadas_cuadros_texto[i]
                                # Saber que cajas hay que borrar usando sus coordenadas
                                # Si la caja se encuentra dentro de la zona pintada hay que borrarla
                                for caja in lista_cajas:
                                    if caja.x >= cuadro[0] and caja.x <= cuadro[0] + cuadro[2] and \
                                        caja.y >= cuadro[1] and caja.y <= cuadro[1] + cuadro[3]:
                                        cajas_a_borrar.append(caja)
                            # Borrar los cuadros de texto de la lista
                            for caja in cajas_a_borrar:
                                lista_cajas.remove(caja)
                            # Borrar las cajas que no han especificado ningun nodo
                            cajas_vacias = []
                            for caja in lista_cajas:
                                if caja.num_nodo == '':
                                    cajas_vacias.append(caja)
                            for caja in cajas_vacias:
                                lista_cajas.remove(caja)
                            # Desplazar las cajas a la posicion que corresponda
                            cajas_libres = []
                            for i, pos in enumerate(posiciones_cuadros_texto):
                                pos_libre = True
                                # Ver en que posiciones de la lista de cajas se encuentran 
                                # las cajas existentes
                                for caja in lista_cajas:
                                    coor = [caja.x, caja.y]
                                    if coor == pos:
                                        pos_libre = False
                                # Si hay un hueco en la lista
                                if pos_libre:
                                    cajas_libres.append(i)
                                    # Buscar la caja que ocupa la siguiente posicion
                                    # ocupada
                                    for pos2 in posiciones_cuadros_texto[i + 1:]:
                                        pos_cambiada = False
                                        for caja in lista_cajas:
                                            coor = [caja.x, caja.y]
                                            if pos2 == coor:
                                                # Buscar la caja de relaciones (si hay)
                                                for caja_rel in lista_cajas:
                                                    if caja_rel.x == coor[0] and caja_rel.y == coor[1] + alto_cajas:
                                                        caja_rel.x = pos[0]
                                                        caja_rel.y = pos[1] + alto_cajas
                                                caja.x = pos[0]
                                                caja.y = pos[1]
                                                pos_cambiada = True
                                                break
                                        if pos_cambiada:
                                            break
                            pygame.draw.rect(globals.screen, white, pygame.Rect(115, 5, 670, 185))
                            pygame.display.flip()
                            # Borrar el boton y el nodo
                            for boton in nodos_borrados:
                                # Borrar el boton y el nodo
                                lista_botones_nodo_inicial.remove(boton)
                                globals.lista_nodos.remove(boton.texto)
                                # Borrar las aristas relacionadas con los nodos borrados
                                for r in globals.lista_relaciones:
                                    if boton.texto in r:
                                        r.remove(boton.texto)      
                                globals.lista_relaciones[int(boton.texto)] = []
                            # Volver a pintar las cajas en su nueva posicion
                            for caja in lista_cajas:
                                caja.draw_borrado()
                            global num_nodos
                            num_nodos -= len(nodos_borrados)
                            nodos_a_borrar[:] = []
                            nodos_borrados[:] = []
                            # Asignar las nuevas coordenadas a los botones
                            # Si ningun nodo se ha borrado las coordenadas seran las mismas
                            # coordenadas_cambiadas --> Al borrar un boton el nuevo deberia
                            #                           crearse en la misma posicion, por lo que hay que
                            #                           asignarle las coordenadas de la siguiente pos
                            coordenadas_cambiadas = False
                            for i, coor in enumerate(coordenadas_botones_nodo_inicial):
                                if i < len(lista_botones_nodo_inicial):
                                    # Pintar la antigua posicion del boton (si
                                    # cambian las coordenadas) de blanco
                                    b = lista_botones_nodo_inicial[i]
                                    pygame.draw.rect(globals.screen, white, pygame.Rect(b.x, b.y, b.size[0], b.size[1]))
                                    # Cambiar las coordenadas
                                    lista_botones_nodo_inicial[i].x = coor[0]
                                    lista_botones_nodo_inicial[i].y = coor[1]
                                else:
                                    if not coordenadas_cambiadas:
                                        global x_nodo_inicial
                                        global y_nodo_inicial
                                        x_nodo_inicial = coor[0]
                                        y_nodo_inicial = coor[1]
                                        coordenadas_cambiadas = True
                                    # Eliminar las coordenadas de los botones borrados
                                    coordenadas_botones_nodo_inicial.remove(coor)
                            # Colocar y colorear los nodos que no se han borrado
                            for i, boton in enumerate(lista_botones_nodo_inicial):
                                global primer_nodo
                                # Si el nodo inicial se ha borrado, se establece
                                # el primero de la lista como nuevo nodo inicial
                                if str(primer_nodo) not in globals.lista_nodos:
                                    primer_nodo = int(globals.lista_nodos[0])
                                # Colorear los botones dependiendo de si se corresponden
                                # con el nodo inicial o no
                                if int(boton.texto) == primer_nodo:
                                    boton.estado = 1
                                else:
                                    boton.estado = 0
                                boton.diseno_boton(boton.colores[boton.estado])
                            # Ocultar el boton de borrar en caso de que no quede ningun nodo
                            if len(globals.lista_nodos) == 0:
                                b = globals.boton_borrar_nodos
                                pygame.draw.rect(globals.screen, white, pygame.Rect(b.x, b.y, b.size[0], b.size[1]))
                                primer_nodo = -1
                                primer_nodo_creado = False

    """ El usuario cancela el borrado de nodos """
    def cancelar_borrado(self, event):
        # Comprueba que el borrado se esta llevando a cabo para
        # poder cancelarlo
        global borrando_nodos
        if borrando_nodos:
            x, y = pygame.mouse.get_pos()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if pygame.mouse.get_pressed()[0]:
                    if globals.boton_cancelar.rect.collidepoint(x, y):
                        borrando_nodos = False
                        for i, boton in enumerate(lista_botones_nodo_inicial):
                            if int(boton.texto) == primer_nodo:
                                boton.estado = 1
                            else:
                                boton.estado = 0
                            boton.diseno_boton(boton.colores[boton.estado])
                        globals.boton_borrar_nodos.diseno_boton(blue)
                        b = globals.boton_cancelar
                        pygame.draw.rect(globals.screen, white, pygame.Rect(b.x, b.y, b.size[0], b.size[1]))
                        print("Cancelar borrado")

    """ Muestra por consola las coordenadas del raton """
    def coordenadas(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            x, y = pygame.mouse.get_pos()
            print("x: ", x,"; y: ", y)

""" Clase botón, muestra un boton para cada nodo creado
Al hace clic el boton cambia de color, indicando que el numero
que muestra el boton sera el nodo en el que empiece la simulacion """
class Boton_Nodo:
    def __init__(self, text, x, y, color):
        self.x = x
        self.y = y
        self.texto = text
        self.active = False
        self.diseno_boton(color)
        # Los botones de los nodos tienen cuatro estados:
        #   0 - El nodo no esta seleccionado como inicial
        #   1 - El nodo esta seleccionado como inicial
        #   2 - El nodo se puede seleccionar para borrar
        #   3 - El nodo se ha seleccionado para borrarlo
        self.estado = 0
        # Color de cada estado
        self.colores = [grey, green, pink, red]
        self.color = self.colores[self.estado]
 
    """ Establece el nuevo color del boton al realizar el usuario
    algun cambio sobre este (elegirlo como nodo inicial o seleccionarlo
    para borrar, por ejemplo) """
    def diseno_boton(self, color):
        self.text = fuente_botones.render(" " + self.texto, 1, white)
        self.size = self.text.get_size()
        self.size = list(self.size)
        self.size[0] = tam_botones_nodo_inicial
        self.surface = pygame.Surface(self.size)
        self.surface.fill(color)
        self.surface.blit(self.text, (0, 0))
        self.rect = pygame.Rect(self.x, self.y, self.size[0], self.size[1])

    def muestra_boton(self):
        globals.screen.blit(self.surface, (self.x, self.y))
 
    def boton_click(self, event, boton):
        x, y = pygame.mouse.get_pos()
        if event.type == pygame.MOUSEBUTTONDOWN:
            if pygame.mouse.get_pressed()[0]:
                if boton.rect.collidepoint(x, y):
                    # Comprueba si se estan pulsando los botones con el
                    # borrado de nodos activado
                    if not borrando_nodos:
                        # Comprueba que no se este pulsando el boton del nodo
                        # inicial
                        if int(self.texto) != primer_nodo:
                            # Desactivar todos los demas botones en la lista
                            for boton in lista_botones_nodo_inicial:
                                boton.desactivar()
                                boton.diseno_boton(boton.colores[0])
                            # Activar este boton
                            self.activar()
                            # Cambiar el color del boton
                            self.diseno_boton(self.colores[1])
                            # Cambiar el nodo inicial
                            cambia_primer_nodo(int(self.texto))
                            print("Nodo inicial: ", primer_nodo)
                    else:
                        # Comprobar el estado del boton
                        # 2 - No se ha seleccionado para borrar
                        # 3 - Se ha seleccionado
                        # Cambiar el color y estado dependiendo del estado
                        if self.estado == 2:
                            boton.estado = 3
                            boton.diseno_boton(boton.colores[boton.estado])
                            nodos_a_borrar.append(self.texto)
                        elif self.estado == 3:
                            boton.estado = 2
                            boton.diseno_boton(boton.colores[boton.estado])
                            nodos_a_borrar.remove(self.texto)

    def desactivar(self):
        self.estado = 0

    def activar(self):
        self.estado = 1

# Clase botón, cada objeto de esta clase se corresponde
# a una de las opciones de ejecucion paso a paso (SI/NO)
class Boton_Paso_a_Paso:
    def __init__(self, text, x, y, color):
        self.x = x
        self.y = y
        self.texto = text
        self.active = False
        self.color_inactive = color
        self.color_active = green
        self.diseno_boton(color)
 
    def diseno_boton(self, color):
        self.text = fuente_botones.render(self.texto, 1, white)
        self.size = self.text.get_size()
        self.size = list(self.size)
        self.size[0] = tam_botones_paso_a_paso
        self.surface = pygame.Surface(self.size)
        self.surface.fill(color)
        self.surface.blit(self.text, (0, 0))
        self.rect = pygame.Rect(self.x, self.y, self.size[0], self.size[1])

    def muestra_boton(self):
        globals.screen.blit(self.surface, (self.x, self.y))
 
    def boton_click(self, event, boton):
        x, y = pygame.mouse.get_pos()
        if event.type == pygame.MOUSEBUTTONDOWN:
            if pygame.mouse.get_pressed()[0]:
                if boton.rect.collidepoint(x, y):
                    # Desactivar el otro boton SI/NO
                    if boton == globals.boton_si:
                        globals.boton_no.desactivar()
                        globals.boton_no.diseno_boton(globals.boton_no.color_inactive)
                        globals.cambia_paso_a_paso(True)
                    else:
                        globals.boton_si.desactivar()
                        globals.boton_si.diseno_boton(globals.boton_si.color_inactive)
                        globals.cambia_paso_a_paso(False)
                    # Activar este boton
                    self.activar()
                    # Cambiar el color del boton
                    self.diseno_boton(self.color_active)
                    # Imprimir paso a paso actual
                    print("Paso a paso: ", globals.paso_a_paso)

    def no_paso_a_paso(self):
        globals.boton_si.desactivar()
        globals.boton_si.diseno_boton(globals.boton_si.color_inactive)
        globals.cambia_paso_a_paso(False)
        # Activar este boton
        self.activar()
        # Cambiar el color del boton
        self.diseno_boton(self.color_active)
        # Imprimir paso a paso actual
        print("Paso a paso: ", globals.paso_a_paso)

    def desactivar(self):
        self.active = False

    def activar(self):
        self.active = True

# Clase cuadro de texto
class InputBox:
    # parametros --> x, y, w, h, text, nodo (true si es el
    #               cuadro de texto de un nodo o false si es de relaciones),
    #               num_nodo (número de nodo al que se aplican las relaciones)
    def __init__(self, x, y, w, h, text, nodo, num_nodo):
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        # Teclas permitidas (numeros y teclado numerico del 0 al 7)
        # Cualquier otra tecla lanza un error
        self.keys = [pygame.K_0, pygame.K_1, pygame.K_2, pygame.K_3, pygame.K_4, pygame.K_5, \
            pygame.K_6, pygame.K_7, pygame.K_KP0, pygame.K_KP1, pygame.K_KP2, pygame.K_KP3, \
            pygame.K_KP4, pygame.K_KP5, pygame.K_KP6, pygame.K_KP7]
        self.rect = pygame.Rect(x, y, w, h)
        # Colores del cuadro de texto, gris si se esta usando
        # o negro si no
        self.color_active = grey
        self.color_inactive = black
        self.color = self.color_inactive
        self.text = text
        self.nodo = nodo
        # Ademas de ser el nodo de origen de las aristas, 
        # indica si el cuadro ha sido usado o no (a la hora de borrar nodos,
        # los cuadros de texto que no han anadido ningun nodo son eliminados)
        # Los cuadros de texto para aristas se dan siempre por usados
        self.num_nodo = num_nodo
        # Texto con las aristas del nodo
        self.relations = ''
        # Solo para los cuadros de texto de nodos, impide que el numero
        # del nodo sea cambiado una vez se haya creado el nodo
        self.usado = False
        self.txt_surface = fuente_cuadros_texto.render(text, 1, self.color)
        self.active = False
        # Se encarga de que el texto solo sea escrito una vez, para
        # que no se superpongan las siguientes escrituras dificultando
        # la lectura, cuando text y text_aux sean diferentes se escribe
        self.text_aux = ''
        self.draw_aux()

    """ Administra los eventos de los cuadros de texto: pulsaciones del
        raton y de teclas """
    def handle_event(self, event):
        # Si se hace click
        if event.type == pygame.MOUSEBUTTONDOWN and not self.usado:
            # Si se hace click en el cuadro de texto
            if self.rect.collidepoint(event.pos):
                self.active = not self.active
            else:
                self.active = False
            # Cambiar el color si el cuadro esta activo o no
            self.color = self.color_active if self.active else self.color_inactive

        # Si se pulsa una tecla usando el cuadro de texto
        elif event.type == pygame.KEYDOWN and self.active:
            # Si se pulsa INTRO
            if event.key == pygame.K_RETURN or event.key == pygame.K_KP_ENTER:
                try:
                    t = self.text
                    if not self.text.isdigit():
                        self.text = 'Err: NaN'
                        raise Exception(f'El dato introducido ({t}) no es un número')
                    elif int(self.text) < 0:
                        self.text = 'Err: num < 0'
                        raise Exception(f'Número introducido ({t}) menor que 0')
                    elif int(self.text) > 7:
                        self.text = 'Err: num > 7'
                        raise Exception(f"Número introducido ({t}) mayor que 7")
                    global y_nodo_inicial
                    global x_nodo_inicial
                    global x_nodo_inicial_aux
                    global num_nodos
                    global lista_botones_nodo_inicial
                    # Si es el cuadro de texto para establecer el número de nodo
                    if self.nodo and not self.usado:
                        if self.text in globals.lista_nodos:
                            self.text = f'Err: {t} ya usado'
                            raise Exception(f'El nodo {t} ya existe')
                        global primer_nodo
                        global primer_nodo_creado
                        # Añadir el nodo a la lista de nodos
                        globals.nuevo_nodo(self.text)
                        print("Nuevo nodo: ", self.text)
                        num_nodos = num_nodos + 1
                        lista_botones_nodo_inicial.append(Boton_Nodo(self.text, x_nodo_inicial, y_nodo_inicial, grey))
                        coordenadas_botones_nodo_inicial.append([x_nodo_inicial, y_nodo_inicial])
                        if not primer_nodo_creado:
                            cambia_primer_nodo(int(self.text))
                            primer_nodo_creado = True
                            lista_botones_nodo_inicial[0].activar()
                            lista_botones_nodo_inicial[0].diseno_boton(lista_botones_nodo_inicial[0].colores[1])
                        x_nodo_inicial += separacion_horizontal_botones_nodo_inicial
                        if num_nodos == 4:
                            x_nodo_inicial = x_nodo_inicial_aux
                            y_nodo_inicial = y_nodo_inicial + separacion_vertical_botones_nodo_inicial
                        # Crear el cuadro de texto para establecer las relaciones del nodo
                        self.num_nodo = self.text
                        input_box = InputBox(self.x, self.y + alto_cajas, ancho_cajas, alto_cajas, '', False, self.text)
                        lista_cajas.append(input_box)
                        # Mostrar número del nodo en pantalla
                        texto_etiqueta = "Nodo: " + self.text
                        label = fuente_info.render(texto_etiqueta, 1, black)
                        globals.screen.blit(label, (self.x, self.y + 2*alto_cajas))
                        self.usado = True
                        self.active = False
                        self.color = self.color_active if self.active else self.color_inactive
                    # Si es el cuadro de texto para establecer las relaciones del nodo
                    elif not self.nodo:
                        # Si el nodo a relacionar no es el propio nodo
                        if self.num_nodo != self.text:
                            if self.text not in globals.lista_nodos:
                                globals.nuevo_nodo(self.text)
                                print("Nuevo nodo: ", self.text)
                                globals.boton_nodo.cuadro_texto(True, self.text)
                                num_nodos = num_nodos + 1
                                lista_botones_nodo_inicial.append(Boton_Nodo(self.text, x_nodo_inicial, y_nodo_inicial, grey))
                                coordenadas_botones_nodo_inicial.append([x_nodo_inicial, y_nodo_inicial])
                                x_nodo_inicial += separacion_horizontal_botones_nodo_inicial
                                if num_nodos == 4:
                                    x_nodo_inicial = x_nodo_inicial_aux
                                    y_nodo_inicial = y_nodo_inicial + separacion_vertical_botones_nodo_inicial
                            # Añadir relación - comprobar que la relación no existe ya
                            # p.e. si se quiere añadir 3-5, comprobar que no existe ya 5-3
                            int_nodo = int(self.num_nodo)
                            int_relacion = int(self.text)
                            # self.text = Nodo a relacionar, nodo de destino
                            # int_nodo = Nodo de origen
                            if self.text not in globals.lista_relaciones[int_nodo] and \
                                str(int_nodo) not in globals.lista_relaciones[int(self.text)]:
                                globals.nueva_relacion(int_nodo, self.text)
                                print(globals.lista_relaciones)
                                print(f"Nueva arista: {self.num_nodo}-{self.text}")
                                if self.relations == '':
                                    self.relations = f'Aristas: {self.num_nodo}-{self.text}'
                                else:
                                    # Comprobar si ya se han escrito las aristas o no,
                                    # para no pintarlas dos veces
                                    for r in globals.lista_relaciones[int_nodo]:
                                        if r not in self.relations:
                                            self.relations = f'{self.relations};{self.num_nodo}-{self.text}'
                            else:
                                self.text = f'Err: {int_nodo}-{t} ya usada'
                                raise Exception(f"La arista {int_nodo}-{t} ya existe")
                            label = fuente_info.render(self.relations, 1, black)                            
                            globals.screen.blit(label, (self.x, self.y + 2 * alto_cajas))
                        else:
                            self.text = 'Err: nodo salida'
                            raise Exception(f"El nodo de salida y de destino ({t}) es el mismo")
                        self.text = ''
                except Exception as e:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    print(f"Excepcion, linea {exc_tb.tb_lineno}: {e}")
            # Si se pulsa la tecla BORRAR se borra el último caracter
            elif event.key == pygame.K_BACKSPACE:
                self.text = self.text[:-1]
            # Si se introduce un numero permitido
            elif not self.usado and event.key in self.keys:
                if self.text:
                    if self.text[0] == "E":
                        self.text = ''
                if len(self.text) == 0:
                    self.text += event.unicode
            # Si se pulsa cualquier otra tecla se notifica como error
            else:
                self.text = 'Err:tecla errónea'

    """ Muestra el texto del cuadro en caso de que haya
        sido modificado """
    def draw(self):
        # Si el texto ha cambiado desde la ultima vez se escribe
        if self.text != self.text_aux or self.usado:
            pygame.draw.rect(globals.screen, white, pygame.Rect(self.x, self.y, self.w, self.h))
            if self.text:
                # Si el texto es un mensaje de error
                if self.text[0] == "E":
                    self.txt_surface = fuente_error.render(self.text, 1, red)
                else:
                    self.txt_surface = fuente_cuadros_texto.render(self.text, 1, black)
            else:
                self.txt_surface = fuente_cuadros_texto.render(self.text, 1, black)
            globals.screen.blit(self.txt_surface, (self.rect.x+5, self.rect.y + 2))
            self.text_aux = self.text
        pygame.draw.rect(globals.screen, self.color, self.rect, 2)

    """ Muestra el cuadro de texto en pantalla """
    def draw_aux(self):
        globals.screen.blit(self.txt_surface, (self.rect.x+5, self.rect.y + 2))
        pygame.draw.rect(globals.screen, self.color, self.rect, 2)

    """ Dibuja el cuadro de texto en caso de que su nodo
        correspondiente no haya sido borrado """
    def draw_borrado(self):
        # Comprobar que solo se pintan las etiquetas al dibujar
        # el cuadro del numero de nodo, de modo que no aparezcan las
        # etiquetas dos veces
        if self.y == posiciones_cuadros_texto[0][1] or self.y == posiciones_cuadros_texto[4][1]:
            label = fuente_info.render("Nodo: ", 1, black)
            label_size = label.get_size()
            globals.screen.blit(label, (self.x - label_size[0], self.y))
            label = fuente_info.render("Aristas: ", 1, black)
            label_size = label.get_size()
            globals.screen.blit(label, (self.x - label_size[0], self.y + alto_cajas))
            label = fuente_info.render(f"Nodo: {self.num_nodo}", 1, black)
            label_size = label.get_size()
            globals.screen.blit(label, (self.x, self.y + 2*alto_cajas))
            if len(globals.lista_relaciones[int(self.num_nodo)]) > 0:
                relaciones = "Aristas: "
                for r in globals.lista_relaciones[int(self.num_nodo)]:
                    relaciones += f"{self.num_nodo}-{r};"
                self.relations = relaciones
                label = fuente_info.render(relaciones[0:len(relaciones) - 1], 1, black)
                globals.screen.blit(label, (self.x, self.y + 2*alto_cajas + label_size[1]))
        self.relations = ''
        self.rect = pygame.Rect(self.x, self.y, self.w, self.h)
        globals.screen.blit(self.txt_surface, (self.rect.x+5, self.rect.y + 2))
        pygame.draw.rect(globals.screen, self.color, self.rect, 2)

# Clase cuadro nuevo nodo
class Nuevo_nodo:
    def __init__(self, x, y, nuevo_nodo, num_nodo):
        self.x = x
        self.y = y
        self.draw(nuevo_nodo, num_nodo)

    def draw(self, nuevo_nodo, num_nodo):
        texto_etiqueta_nodo = "Nodo: "
        label = fuente_info.render(texto_etiqueta_nodo, 1, black)
        label_size = label.get_size()
        globals.screen.blit(label, (self.x - label_size[0], self.y))
        texto_etiqueta_relaciones = "Aristas: "
        label = fuente_info.render(texto_etiqueta_relaciones, 1, black)
        label_size = label.get_size()
        globals.screen.blit(label, (self.x - label_size[0], self.y + alto_cajas))
        # Añadir el cuadro de texto a la lista
        input_box = InputBox(self.x, self.y, ancho_cajas, alto_cajas, num_nodo, True, num_nodo)
        lista_cajas.append(input_box)
        # Añadir las coordenadas del cuadro de texto y etiquetas
        # para que esten disponibles al borrar los nodos
        coordenadas_cuadros_texto.append([self.x, self.y, ancho_cajas, 2*alto_cajas])
        # Si el parametro nuevo_nodo es True significa que se ha creado
        # un nuevo nodo desde el cuadro de aristas
        if nuevo_nodo:
            input_box.usado = True
            # Crear el cuadro de texto para establecer las relaciones del nodo
            input_box = InputBox(self.x, self.y + alto_cajas, ancho_cajas, alto_cajas, '', False, num_nodo)
            lista_cajas.append(input_box)
            # Mostrar número del nodo en pantalla
            texto_etiqueta = "Nodo: " + num_nodo
            label = fuente_info.render(texto_etiqueta, 1, black)
            globals.screen.blit(label, (self.x, self.y + 2*alto_cajas))

def funcion_principal():
    # Inicializacion de Pygame 
    pygame.init() 

    pygame.display.set_caption("Circuito")

    # Color inicial: blanco
    globals.screen.fill(white)
    pygame.display.flip()

    global fuente_botones
    global fuente_info
    global fuente_cuadros_texto
    global fuente_programa
    global fuente_error
    global fuente_etiquetas
    fuente_botones = pygame.font.SysFont("Helvetica", 20)
    fuente_info = pygame.font.SysFont("Helvetica", 16)
    fuente_etiquetas = pygame.font.SysFont("Helvetica", 16, True)
    fuente_cuadros_texto = pygame.font.SysFont("Helvetica", 16, True)
    fuente_programa = pygame.font.SysFont("Helvetica", 14)
    fuente_error = pygame.font.SysFont("Helvetica", 14)

    # Botones de la interfaz
    globals.boton_nodo = Boton("Nuevo nodo", x_botones_izq, y_botones_izq, blue)
    globals.boton_fichero = Boton("Leer fichero", x_botones_izq, y_botones_izq + separacion_vertical_botones, blue)
    globals.boton_simulacion = Boton("Simulación", x_botones_izq, globals.boton_fichero.y + separacion_vertical_botones, blue)
    globals.boton_restaurar = Boton(" Restaurar", x_botones_izq, globals.boton_simulacion.y + separacion_vertical_restaurar, blue)
    globals.boton_borrar_nodos = Boton(" Borrar nodos", x_nodo_inicial, y_nodo_inicial + 65, blue)
    globals.boton_cancelar = Boton("Cancelar", x_nodo_inicial, y_nodo_inicial + 95, blue)
    globals.boton_siguiente_paso = Boton(" Sig. paso ", x_nodo_inicial_aux, y_nodo_inicial + 190, blue)
    globals.boton_si = Boton_Paso_a_Paso("Sí", x_nodo_inicial_aux, y_nodo_inicial + 160, grey)
    globals.boton_no = Boton_Paso_a_Paso("No", x_nodo_inicial_aux + 55, y_nodo_inicial + 160, grey)

    # Deja como activo el boton de no ejecutar paso a paso
    globals.boton_no.activar()
    globals.boton_no.diseno_boton(globals.boton_no.color_active)

    muestra_etiquetas()

    # fin del programa
    fin = False

    while not fin:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                fin = True
            globals.boton_nodo.boton_click(event, False, '')
            globals.boton_nodo.coordenadas(event)
            globals.boton_fichero.get_file(event)
            globals.boton_simulacion.boton_terminar(event)
            globals.boton_restaurar.restaurar_datos(event)
            globals.boton_borrar_nodos.borrar_nodos(event)
            globals.boton_cancelar.cancelar_borrado(event)
            globals.boton_si.boton_click(event, globals.boton_si)
            globals.boton_no.boton_click(event, globals.boton_no)
            for box in lista_cajas:
                box.handle_event(event)
            for boton in lista_botones_nodo_inicial:
                boton.boton_click(event, boton)
        for box in lista_cajas:
            box.draw()
        for boton in lista_botones_nodo_inicial:
            boton.muestra_boton()
        globals.boton_nodo.muestra_boton(globals.boton_nodo)
        globals.boton_fichero.muestra_boton(globals.boton_fichero)
        globals.boton_simulacion.muestra_boton(globals.boton_simulacion)
        if len(lista_botones_nodo_inicial) > 0:
            label = fuente_programa.render("_______________", 1, black)                            
            globals.screen.blit(label, (9, 84))
            globals.boton_restaurar.muestra_boton(globals.boton_restaurar)
            globals.boton_borrar_nodos.muestra_boton(globals.boton_borrar_nodos)
        elif file != '' or globals.paso_a_paso:
            label = fuente_programa.render("_______________", 1, black)                            
            globals.screen.blit(label, (9, 84))
            globals.boton_restaurar.muestra_boton(globals.boton_restaurar)
        else:
            b = globals.boton_restaurar
            pygame.draw.rect(globals.screen, white, pygame.Rect(b.x, b.y, b.size[0], b.size[1]))
        if borrando_nodos:
            globals.boton_cancelar.muestra_boton(globals.boton_cancelar)
        globals.boton_si.muestra_boton()
        globals.boton_no.muestra_boton()
        pygame.display.update()
    pygame.quit()

if __name__ == "__main__":
    funcion_principal()